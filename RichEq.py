# -*- coding: utf-8 -*-
"""
Finite difference solution of Richards' equation for variably saturated flow
using Picard iterations as per Celia et al. (1990). WRR. 

Developed for GEO585: Hydrologic Modeling

Marco Maneta, University of Montana, marco.maneta@umontana.edu

"""

import AuxFuncs as aux
import numpy as np
import argparse


def RichardsEquation(**kwargs):
    #######################################
    # Unpack arguments
    #######################################

    num_nodes = kwargs['num_nodes'] # number of nodes in soil column
    dx = kwargs['dx'] # array with distance between nodes (m)
    dt = kwargs['dt']  # time step (secs)
    t = 0
    tend = kwargs['total_simulation_time_seconds']  # Simulation length (second)
    max_dt = kwargs['adaptive_max_dt']  # maximum allowed time step (seconds)
    min_dt = kwargs['adaptive_min_dt']  # minimum allowed time step (hr)
    mult_fact = kwargs['adaptive_dt_multiplier']  # factor by which to increase dt if convergence is fast
    div_fact = kwargs['adaptive_dt_reducer']  # factor by which to decrease dt if convergence is slow


    # Create list of report times
    report_times = kwargs['report_interval']
    if not isinstance(report_times, list):
        #report_times = np.linspace(t+report_times, tend+1, (tend-t)/report_times)
        report_times = np.arange(t+report_times, tend+1, report_times)

    report_times = np.asarray(report_times)
    
    # Flag for constitutive relationship model
    theta_psi_model = 1  # 0 - Brook and Corey; 1 - Van Genuchten
    k = kwargs['Ksat']
    if isinstance(k, list):
        Ks = k
    else:
        Ks = np.ones_like(dx) * k  # Saturated hydraulic conductivity  (m s-1)
    
    kwargs['Ksat'] = Ks

    kwargs['theta_psi_flag'] = 1 # use van genuchten functions always
    vgalpha = kwargs['van_genuchten_alpha']
    vgn = kwargs['van_genuchten_n']
    vgm = 1. - 1. / vgn
    kwargs['van_genuchten_m'] = vgm
    #############################
    thetas = kwargs['soil_porosity']  # Soil porosity
    thetar = kwargs['soil_residual_moisture']  # Residual soil moisture
    #####################
    # Boundary and initial conditions
    #####################
    # Type of top BC
    BC_top = kwargs['type_of_boundary_condition']  # 0 if Dirichlet, 1 if Neumann

    xt_d = kwargs['top_dirichlet_boundary_condition']  # WATER POTENTIAL AT THE SURFACE (M) FOR DIRICHLET BC
    xt_n = kwargs['top_neumann_boundary_condition']  # WATER FLUX FROM THE LEAF (M/s) FOR NEUMANN BC (negative: incoming flux)
    xb = kwargs['bottom_dirichlet_boundary_condition']  # WATER POTENTIAL AT THE BOTTOM OF THE SOIL

    xi = kwargs['initial_conditions']
    if isinstance(xi, list):
        xinit = xi
    else:
        xinit = np.ones(num_nodes) * xi  # np.linspace(xt_d, xb, num_nodes)#np.array([-3.7e+02,-3.7e+02, -3.7e+02, -3.7e+02, -3.7e+02, -2.61e+02, -2.61e+02, -2.60e+02, -2.60e+02, -2.59e+02, -2.58e+02, -2.57e+02, -2.56e+02,	-2.55e+02,	-2.54e+02 ])#-370*np.ones(num_nodes)

    #######################################
    #Construct numerical gradient operator G
    #######################################
    Gplus = np.diag(-np.ones(num_nodes)) + np.diag(np.ones((num_nodes-1)),1)
    Gplus[-1, -1] = -1.
    Gplus *= 1/dx

    Gminus = np.diag(-np.ones(num_nodes-1),-1) + np.diag(np.ones((num_nodes)))
    Gminus[0,0] = 1.
    if BC_top == 1:
        Gminus[0, 0] = 0
        Gminus[0, 1] = 0
    Gminus *= 1/dx

    #######################################
    #Construct differential operator for Kbar
    #######################################
    D = np.diag(-np.ones(num_nodes)) + np.diag(np.ones((num_nodes-1)),1)
    D = np.append(D, np.zeros((num_nodes, 1)), axis=1)
    D[-1, -1] = 1


    # Dirichlet type boundary conditions at the bottom and dirichlet or neuman at top
    BCplus = np.zeros((num_nodes))
    BCminus = np.zeros((num_nodes))
    #BCplus = np.deecopy(BC)
    #BCminus = np.copy(BC)
    BCplus[-1] = xb/dx
    BCminus[0] = -xt_d/dx
    if BC_top == 1:
        xt_d = 0.0 # zero out the top dirichlet value so it does no harm
        BCminus[0] = 0

    # Initial conditions
    x = xinit
    thetan = aux.theta(xinit, **kwargs)

    #Create results array
    Theta = thetan
    Psi = xinit[np.newaxis]
    E = np.zeros((num_nodes))
    Time = np.zeros(1)
    #Stopping criteria for Picard iteration
    stop = 0

    #start time-stepping loop or convergence toward steady state
    while t < tend:
        it = 1
        #start Picard iteration
        while stop == 0:
            if t+dt > report_times[0]:
                dt = report_times[0] - t

            # Calculate unsaturated hydr cond
            K = aux.K(x, **kwargs)
            # Calculate capacitance (dpsi/dthedta)
            Cs = aux.Csoil(x, **kwargs)
            # calculate soil moisture at current tension states
            thetan1 = aux.theta(x, **kwargs)

            # calculate interblock hydraulic conductivity
            Kbar = aux.Kbar(K)
            Kbarplus = np.diag(Kbar[1:])
            Kbarminus = np.diag(Kbar[:-1])

            if BC_top == 1:
                #-2*(xt_n-K[1])/K[1]
                #Kbar[0] = 0
                D[0, 0] = 0
                Kbarminus[0, 0] = 0#Kbar[0]/4
                BCminus[0] = xt_n #/Kbarminus[0,0]


            #Construct left hand side matrix
            lhs = (dx/dt)*np.diag(Cs)  - ( np.dot(Kbarplus, Gplus) - np.dot(Kbarminus, Gminus))

            if BC_top == 1:
                Kbarminus[0, 0] = 1 #Kbar[0]/4
            # Construct right hand side
            rhs = (np.dot(Kbarplus, np.dot(Gplus,x) + BCplus) - np.dot(Kbarminus, np.dot(Gminus,x) + BCminus)) - np.dot(D,Kbar) - (dx/dt)*(thetan1 - thetan)
            #if BC_top==1:
            #    rhs[0] -= 2*xt_n + Kbar[0]

            deltah = np.linalg.solve(lhs, rhs)
            #print deltah


            if(np.linalg.norm(deltah)<0.0000001):
                stop = 1
                t += dt
                print "Convergence in ",  it, " iterations with norm ", np.linalg.norm(deltah)

            x1 = x + deltah
            x = x1

            #print "internal iteration number ", it, "with norm " , np.linalg.norm(deltah)
            if(it<4 and stop == 1):
                print "Convergence is fast, increasign dt"
                dt = np.min((dt*mult_fact, max_dt))
                print "new dt is ", dt
                #x = Psi[-1,:]
                #it=0
            if(it>150):
                print "Convergence is slow, decreasign dt"
                dt = np.max((dt*div_fact, min_dt))
                print "new dt is ", dt
                x = Psi[-1,:]
                it=0
            it+=1
            if np.abs(dt - min_dt) < 1e-7:
				print "Convergence not achieved with minimum allowed time step, ", min_dt
				raise RuntimeError("Converge not achieved with minimum allowed time step ", min_dt)



        thetan = thetan1

        if np.isclose(t, report_times).any():
            Time = np.append(Time, t)
            Theta = np.vstack((Theta, thetan1))
            Psi = np.vstack((Psi, x))
            E = np.vstack((E,np.dot(Kbarminus, np.dot(Gminus,x) + Kbar[1:] )))
            if report_times.size > 1:
                report_times = np.delete(report_times, 0)

        print "Calculated ", t, " seconds (", t/3600., " hours)"
        stop = 0

        ## collect information in a dictionary and return the object and also write it to disk as text files
    # save to disk all information
    ts = Time[np.newaxis,:]
    zs = np.arange(0, num_nodes+1)[:,np.newaxis]
    Theta_out = np.concatenate((ts, Theta.T), axis=0)
    Theta_out = np.concatenate((zs, Theta_out), axis=1)
    np.savetxt('SoilMoisture.txt', Theta_out, header="# Soil moisture solution, first row are timesteps, first column soil dephts")

    Psi_out = np.concatenate((ts, Psi.T), axis=0)
    Psi_out = np.concatenate((zs, Psi_out), axis=1)
    np.savetxt('SoilPotential.txt', Psi_out, header="# Soil water potential solution, first row are timesteps, first column soil dephts")
    
    E_out = np.concatenate((ts, E.T), axis=0)
    E_out = np.concatenate((zs, E_out), axis=1)
    np.savetxt('SoilWaterFlux.txt', E_out, header="# Fluxes solution, first row are timesteps, first column soil dephts")

    return 0

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="One-dimensional finite different solution of "
    "the mixed form of Richards equation using Picard iterations as per Celia et al, 1990, WRR."
    "The program accepts constant head (Dirichlet) or constant flux (Neumann) bounday conditions at the top "
    "boundary and constant head boundary conditions at the bottom boundary. Parameters and initial conditions "
    "can be distributed or constant along the domain. The model uses the van Genuchten constitutive relationship "
    "to relate water potential and water content",
    epilog="The program was developed for educational purposes at the University of Montana by marco.manet@umontana.edu")
    
    parser.print_help()
    parser.add_argument("config_filename", type=str, help = "File name with valid configuration file")
    


    args = parser.parse_args()

    print args.config_filename


    with open(args.config_filename) as f:
        d = eval(f.read())
    
    RichardsEquation(**d)

    
