# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 15:43:05 2020

@author: aidan
"""

import matplotlib.pyplot as plt
import numpy as np
import Richards_Equation_Class as REC
from IPython.display import clear_output


## For couse soil
#0.0250 .3660 .04301 .52060
## For medium soil
#0.010 0.392 0.0249 1.1689 

num_nodes = 40
dx = .1
dt = 300.0
time_end = 3*3600
max_dt = 36
min_dt = 1.
adaptive_dt_multiplier = 2.0
adaptive_dt_reducer = .5
van_gen_alpha = .0249
van_gen_n =  1.1689
soil_resid_moisture = .010
soil_porosity = .392
K_saturated = 0.000000922

model = REC.Richards_Equation(num_nodes, dx, dt, max_dt, min_dt,
                 adaptive_dt_multiplier, adaptive_dt_reducer,
                 van_gen_alpha, van_gen_n, soil_resid_moisture, 
                 soil_porosity, K_saturated)
                 

BC_top = 0
BC_bot = 0

BC_bot_val = -.6
BC_top_val = -.5 

head_init = np.ones(num_nodes)*BC_bot_val

#x = np.linspace(-10, 20, 100)
#plt.plot(x, model._soil_moisture_van_genutchen(x))
#plt.figure()
#plt.plot(x, model._dsoil_moisture_van_genutchen(x))


times, heads, moistures, depths = model.solve_richards_eq(head_init, BC_top, BC_top_val, BC_bot, BC_bot_val, time_end)

plt.figure(figsize = (20,10))

for i in range(len(times)):
    plt.subplot(121)
    plt.title('Example 2 Top Diriclet BC')
    plt.plot(heads[i], depths)
    plt.xlabel('Head (m)', fontsize = 18)
    plt.ylabel('Height above datum (m)', fontsize = 18)
    plt.xlim(-.61, -.49)
    plt.subplot(122)
    plt.plot(moistures[i], depths)
    plt.xlabel('Soil Moisture Content', fontsize = 18)
    plt.ylabel('Height above datum (m)', fontsize = 18)
    plt.show()
    print('Average Soil Moisture = ', np.mean(moistures[i]), ' Max Soil Moisture = .368')
    print('The Current time is ', times[i]/3600, 'hrs Out of 3 hrs')

num_nodes = 40
dx = 2.5
dt = 300.0
time_end = 6*3600
max_dt = 3600
min_dt = 1.
adaptive_dt_multiplier = 2.0
adaptive_dt_reducer = .5
van_gen_alpha = .0335
van_gen_n =  1.25
soil_resid_moisture = .102
soil_porosity = .368
K_saturated = 0.00922

model = REC.Richards_Equation(num_nodes, dx, dt, max_dt, min_dt,
                 adaptive_dt_multiplier, adaptive_dt_reducer,
                 van_gen_alpha, van_gen_n, soil_resid_moisture, 
                 soil_porosity, K_saturated)
                 

BC_top = 1
BC_bot = 0

head_bot = -60.
head_top = 7.0e-4 

head_init = np.ones(num_nodes)*head_bot

#x = np.linspace(-10, 20, 100)
#plt.plot(x, model._soil_moisture_van_genutchen(x))
#plt.figure()
#plt.plot(x, model._dsoil_moisture_van_genutchen(x))

times, heads, moistures, depths = model.solve_richards_eq(head_init, BC_top, head_top, BC_bot, head_bot, time_end)
print(head_init)

plt.figure(figsize = (20,10))

for i in range(len(times)):
    clear_output(wait = True)

    plt.subplot(121)
    plt.title('Example 2 Top Neuman BC')
    plt.plot(heads[i], depths)
    plt.xlabel('Head (m)', fontsize = 18)
    plt.ylabel('Height above datum (m)', fontsize = 18)
    plt.xlim(-61, -19)
    plt.subplot(122)
    plt.plot(moistures[i], depths)
    plt.xlabel('Soil Moisture Content', fontsize = 18)
    plt.ylabel('Height above datum (m)', fontsize = 18)
    plt.show()
    print('Average Soil Moisture = ', np.mean(moistures[i]), ' Max Soil Moisture = .368')
    print('The Current time is ', times[i]/3600, 'hrs Out of 6 hrs')

