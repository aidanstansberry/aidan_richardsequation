# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 15:11:29 2015

@author: marco
"""
import numpy as np

"""
Grid configuration and time stepping information
"""

num_nodes = 40
dx = 2.5 #array with distance between nodes (m)
t = 0
tend = 24. # Simulation length (hrs)


#Adaptive time stepping information
dt = 300. #time step (hr)
max_dt = 3600. #maximum allowed time step (hr)
min_dt = 1. #minimum allowed time step (hr)
mult_fact = 2. #factor by which to increase dt if convergence is fast
div_fact = 0.90 #factor by which to decrease dt if convergence is slow 


"""
#######################
# MODEL PARAMETERS
#######################
"""
"""
SOIL SPECIFIC PARAMETERS
"""
# Flag for constitutive relationship model
theta_psi_model = 1 # 0 - Brook and Corey; 1 - Van Genuchten
Ks = np.ones_like(dx)
Ks *= 0.00922000000000 # Saturated hydraulic conductivity  (m s-1)
#############################
# If theta_psi_model = 0
bclam = 7.3 # Brooks and Corey lambda
psiae = 0.480 # Soil air entry pressure (m)
#############################
# If theta_psi_model = 1
vgalpha = 0.03350000000000000 #parameters for sandy loam soils
vgn = 1.250000000000000000000
vgm = 1.-1./vgn
#############################
thetas = 0.368000000000000 # Soil porosity
thetar = 0.102000000000000 # Residual soil moisture
#####################
# Boundary and initial conditions
#####################
#Type of top BC
BC_top = 0 # 0 if Dirichlet, 1 if Neumann

xt_d = -50. # WATER POTENTIAL AT THE SURFACE (M) FOR DIRICHLET BC
xt_n = 5.e-4 # WATER FLUX FROM THE LEAF (M/s) FOR NEUMANN BC (negative: incoming flux)
xb = -60. # WATER POTENTIAL AT THE BOTTOM OF THE SOIL 

xinit = np.ones(num_nodes)*-50.#np.linspace(xt_d, xb, num_nodes)#np.array([-3.7e+02,-3.7e+02, -3.7e+02, -3.7e+02, -3.7e+02, -2.61e+02, -2.61e+02, -2.60e+02, -2.60e+02, -2.59e+02, -2.58e+02, -2.57e+02, -2.56e+02,	-2.55e+02,	-2.54e+02 ])#-370*np.ones(num_nodes)

"""
#######################################
# NO NEED TO TOUCH ANYTHING BELOW THIS LINE
#######################################
"""

#Pack arguments
argums = {
'theta_psi_flag':theta_psi_model,
'psiae': psiae,
'lam' : bclam, 
'vgalpha': vgalpha,
'vgn': vgn,
'vgm': vgm,
'thetas': thetas,
'thetar': thetar,
'Ksat': Ks,
'num_nodes': num_nodes,
'bottom_BC': xb,
'top_BC': xt_d
}