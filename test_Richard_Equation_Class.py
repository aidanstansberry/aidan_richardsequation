#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 16:55:45 2020

@author: aidan
"""
import nose
import numpy as np
import Richards_Equation_Class as REC
class test_Richards_Equation(object):
    
    @classmethod
    def setup_class(cls):
        """This method is run once of each class before any tests are run """
        cls.num_nodes = 10
        cls.dx = .02
        cls.dt = 1.0
        cls.max_dt = 3.0
        cls.min_dt = .1
        cls.adaptive_dt_multiplier = 2.5
        cls.adaptive_dt_reducer = .25
        cls.van_gen_alpha = 2.2
        cls.van_gen_n = 2.2
        cls.van_gen_m = 1 - 1/cls.van_gen_n
        cls.soil_residual_moisture = .04
        cls.soil_porosity = .3
        cls.K_saturated = .0092 
        
    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass
    
    def setUP(self):
        """this method is run once before __each__ test is run"""
        pass
    
    def teardown(self):
        """this method is run once after __each__ test is run"""
        pass
    
    def test_init(self):
        
        soil = REC.Richards_Equation(self.num_nodes, self.dx, self.dt, self.max_dt, self.min_dt,
                 self.adaptive_dt_multiplier, self.adaptive_dt_reducer,
                 self.van_gen_alpha, self.van_gen_n, self.soil_residual_moisture, 
                 self.soil_porosity, self.K_saturated)
        
        nose.tools.assert_equal(soil.num_nodes, self.num_nodes)
        nose.tools.assert_equal(soil.dx, self.dx)
        nose.tools.assert_equal(soil.dt, self.dt)
        nose.tools.assert_equal(soil.max_dt, self.max_dt)
        nose.tools.assert_equal(soil.min_dt, self.min_dt)
        nose.tools.assert_equal(soil.adaptive_dt_multiplier, self.adaptive_dt_multiplier)
        nose.tools.assert_equal(soil.adaptive_dt_reducer, self.adaptive_dt_reducer)
        
    def test_forward_diff(self):

        soil = REC.Richards_Equation(self.num_nodes, self.dx, self.dt, self.max_dt, self.min_dt,
                 self.adaptive_dt_multiplier, self.adaptive_dt_reducer,
                 self.van_gen_alpha, self.van_gen_n, self.soil_residual_moisture, 
                 self.soil_porosity, self.K_saturated)
        
        
        matrix = soil._forward_diff()
        
        test_matrix = np.zeros((self.num_nodes, self.num_nodes))
        
        for i in range(self.num_nodes):
            for j in range(self.num_nodes):
                if i == (j):
                    test_matrix[i, j] = -1
                if i == (j-1):
                    test_matrix[i, j] = 1
        print(test_matrix)
        print(matrix)
        Truth = (matrix == test_matrix).all()        
        nose.tools.assert_equal(Truth, True)
        
    def test_backward_diff(self):

        soil = REC.Richards_Equation(self.num_nodes, self.dx, self.dt, self.max_dt, self.min_dt,
                 self.adaptive_dt_multiplier, self.adaptive_dt_reducer,
                 self.van_gen_alpha, self.van_gen_n, self.soil_residual_moisture, 
                 self.soil_porosity, self.K_saturated)
        
        
        matrix = soil._backward_diff()
        
        test_matrix = np.zeros((self.num_nodes, self.num_nodes))
        
        for i in range(self.num_nodes):
            for j in range(self.num_nodes):
                if i == (j+1):
                    test_matrix[i, j] = -1
                if i == (j):
                    test_matrix[i, j] = 1
        print(test_matrix)
        print(matrix)
        Truth = (matrix == test_matrix).all()        
        nose.tools.assert_equal(Truth, True)
        
        
    def test_soil_moisture_van_genutchen(self):
        
        soil = REC.Richards_Equation(self.num_nodes, self.dx, self.dt, self.max_dt, self.min_dt,
                 self.adaptive_dt_multiplier, self.adaptive_dt_reducer,
                 self.van_gen_alpha, self.van_gen_n, self.soil_residual_moisture, 
                 self.soil_porosity, self.K_saturated)
                                     
        heads = np.array([-.5, -.2, 1.3, 1.67])
        
        soil_moisture = soil._soil_moisture_van_genutchen(heads)
                                     
        soil_moisture2 = (self.soil_porosity - self.soil_residual_moisture)/(1. + (self.van_gen_alpha*np.abs(heads))**self.van_gen_n)**self.van_gen_m + self.soil_residual_moisture
        
        soil_moisture2[2:] = self.soil_porosity
        
        for i in range(len(soil_moisture2)):
            
            nose.tools.assert_almost_equal(soil_moisture2[i], soil_moisture[i], 6)
        
    def test_dsoil_moisture_van_genutchen(self):
        
        soil = REC.Richards_Equation(self.num_nodes, self.dx, self.dt, self.max_dt, self.min_dt,
                 self.adaptive_dt_multiplier, self.adaptive_dt_reducer,
                 self.van_gen_alpha, self.van_gen_n, self.soil_residual_moisture, 
                 self.soil_porosity, self.K_saturated)
                                     
        heads = np.array([-.5, -.2, 1.3, 1.67])
    
        soil_moisture = soil._dsoil_moisture_van_genutchen(heads)
        print(soil_moisture)        
        
        
        soil_moisture2 = -self.van_gen_alpha*self.van_gen_n*np.sign(heads)*(1./self.van_gen_n-1.)*(self.van_gen_alpha*np.abs(heads))**(self.van_gen_n-1.)*(self.soil_residual_moisture -self.soil_porosity)*((self.van_gen_alpha*np.abs(heads))**self.van_gen_n + 1)**(1./self.van_gen_n-2.)
        
        soil_moisture2[2:] = 0.0
        print(soil_moisture2)

        for i in range(len(soil_moisture2)):
            
            nose.tools.assert_almost_equal(soil_moisture2[i], soil_moisture[i], 6)
            
            
    def test_hydraulic_conductivity_van_genutchen(self):

        soil = REC.Richards_Equation(self.num_nodes, self.dx, self.dt, self.max_dt, self.min_dt,
                 self.adaptive_dt_multiplier, self.adaptive_dt_reducer,
                 self.van_gen_alpha, self.van_gen_n, self.soil_residual_moisture, 
                 self.soil_porosity, self.K_saturated)
                                     
        heads = np.array([-.5, -.2, 1.3, 1.67])
    
        K = soil._hydraulic_conductivity_van_genutchen(heads)
        
        S = 1./(1. + (self.van_gen_alpha*np.abs(heads))**self.van_gen_n)**self.van_gen_m        
        
        S[2:] = 1.0        
        
        K2 = self.K_saturated*S**(1./2.)*(1. - (1. - S**(1./self.van_gen_m))**self.van_gen_m)**2.

        for i in range(len(K2)):
            
            nose.tools.assert_almost_equal(K2[i], K[i], 6)
            
            
    def test_K_interblock(self):
        
        soil = REC.Richards_Equation(self.num_nodes, self.dx, self.dt, self.max_dt, self.min_dt,
                 self.adaptive_dt_multiplier, self.adaptive_dt_reducer,
                 self.van_gen_alpha, self.van_gen_n, self.soil_residual_moisture, 
                 self.soil_porosity, self.K_saturated)

        heads = np.array([-.5, -.2, 1.3, 1.67])
    
        K = soil._hydraulic_conductivity_van_genutchen(heads)      
        
        K_interblock = soil._K_interblock(K)        
        
        for i in range(len(K_interblock)):
            
            Kintertest = (K[i]+K[i+1])/2            
            
            nose.tools.assert_almost_equal(K_interblock[i], Kintertest)
        
        
        