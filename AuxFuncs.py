# -*- coding: utf-8 -*-
"""
Created on Sat May 16 16:28:42 2015

@author: marcomaneta
"""
import numpy as np

def Csoil (x, **params):
    """
    Derivative of soil moisture - soil potential curve as described by the
    Brooks and Corey Model and for plant water content - hydraulic potential
    as described by Sperry (1998)
    Parameters:
    x: current soil hydraulic potential (m)
    psiae: soil air entry pressure (m)
    lam: Brooks and Corey lambda exponent
    thetas: soil porosity
    thetar: soil residual moisture content
    LA: leaf area (m2)
    a: fraction of total plant volume with which the node exchanges water
    num_soil_nodes:
    num_plant_nodes:
    """
    theta_psi_model=params['theta_psi_flag']
    #psiae = params['psiae']
    #lam = params['lam']
    alpha = params['van_genuchten_alpha']
    n = params['van_genuchten_n']
    m = params['van_genuchten_m']
    thetas = params['soil_porosity']
    thetar = params['soil_residual_moisture']
    
    
    xsoil = x
  
    if theta_psi_model == 0:
        C_s = np.piecewise(xsoil, [xsoil<-psiae], 
                         [lambda xsoil: -(thetas - thetar)*np.power(psiae/np.abs(xsoil) , 1/lam) / (lam*np.abs(xsoil)),0.])
    if theta_psi_model == 1:
        C_s = np.piecewise(xsoil, [xsoil<0],
                        [lambda xsoil: -alpha*n*np.sign(xsoil)*(1./n-1.)*(alpha*np.abs(xsoil))**(n-1.)*(thetar-thetas)*((alpha*np.abs(xsoil))**n + 1)**(1./n-2.),0.])
    
    
    return C_s
        


def K(x, **params):
    """
    Returns the hydraulic conductivity corresponding to a given 
    hydraulic potential in the soil-plant column\n
    Parameters:\n
    x: currents soil hydraulic potential\n
    van_genuchten_alpha: van Genuchten parameter alpha
    van_genuchten_n: van Genuchten parameter n (-)
    Ksat: saturated hydraulic conductivity (m/h)\n
    num_soil_nodes: number of soil nodes
    num_plant_nodes:
    """
    #unpack params
    theta_psi_model=params['theta_psi_flag']
    #psiae = params['psiae']
    #lam = params['lam']
    alpha = params['van_genuchten_alpha']
    n = params['van_genuchten_n']
    m = params['van_genuchten_m']
    
    Ksat = params['Ksat']
    xb = params['bottom_dirichlet_boundary_condition']
    xt = params['top_dirichlet_boundary_condition']
    
    
    #insert bc to calculate its corresponding K    
    x = np.pad(x,1, 'constant', constant_values=[xt,xb])
         
    xsoil = x
    
    if theta_psi_model == 0:
        S = np.piecewise(xsoil, [xsoil<-psiae],
                     [lambda xsoil: np.power(psiae/np.abs(xsoil), 1/lam), 1.])
        p = 2.*lam + 3. # hydraulic conductivity exponent    
        temp2 = Ksat * np.power(S, p)
        
    
    if theta_psi_model == 1:
        S = np.piecewise(xsoil, [xsoil<0],
                     [lambda xsoil: 1./(1. + (alpha*np.abs(xsoil))**n)**m,1.])
        temp2 = Ksat*S**(1./2.)*(1. - (1. - S**(1./m))**m)**2.
    
    
    return temp2
    
   
    
def Kbar(K):
    """
    Calculate the K at the cell faces as the harmonic average of K at the cell centers\n
    K: vector of hydraulic conductivities at n cell centers\n
    
    returns:\n
    n+1 vector of hydraulic conductivity at faces\n
    
    Values for first and last face are are extrapolated using nearest neighbor
    """
    #Pad K on each end with first and last value of hydraulic conductivity    
    #Kaug = np.insert(K,(0,-1),K[np.array([0,-1])] )

    
 
    #return np.delete(1 /(0.5*((1/K) + (1/np.roll(K,-1)))), -1 )
    return np.delete(0.5*(K + (np.roll(K,-1))), -1 )

def theta(x, **params):
    
    theta_psi_model=params['theta_psi_flag']    
    #psiae = params['psiae']
    #lam = params['lam']
    alpha = params['van_genuchten_alpha']
    n = params['van_genuchten_n']
    m = params['van_genuchten_m']
    thetas = params['soil_porosity']
    thetar = params['soil_residual_moisture']
    
       
    xsoil = x
    
    if theta_psi_model == 0:    
        S = np.piecewise(xsoil, [xsoil<-psiae],
                     [lambda xsoil: (psiae/np.abs(xsoil))**(1./lam), 1.])
        theta_s = S * (thetas - thetar) + thetar 
        
    if theta_psi_model == 1:
        theta_s = np.piecewise(xsoil, [xsoil<0],
                [lambda xsoil: (thetas - thetar)/(1. + (alpha*np.abs(xsoil))**n)**m + thetar, thetas])
    
    return theta_s
    


